<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            /*
             * Creating foreign key
             */

            $table->unsignedInteger('category_id')->nullable()->default(null);
            $table->foreign('category_id')->references('id')->on('categories');

            /*
             * End of creating foreign key
             */

            $table->string('name');
            $table->string('slug');
            $table->string('price');
            $table->boolean('is_active')->default(1);
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
