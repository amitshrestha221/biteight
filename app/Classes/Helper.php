<?php
/**
 * Created by PhpStorm.
 * User: amit
 * Date: 7/17/19
 * Time: 7:20 AM
 */

namespace App\Classes;


use App\Category;

class Helper
{
    public static function getCategories(){
        $categories = Category::where('is_active', 1)->get();
        return $categories;
    }

    public static function getCartCount(){
        if(session()->has('cart')){
            $cart_items = session('cart');
            $product_count = count($cart_items);

            $product_price = 0;

            for($i=0; $i<count($cart_items); $i++){
                $product_price += $cart_items[$i]['product_id']['price'];
            }

            $count = ['product_count' => $product_count, 'product_price' => $product_price];
        }else{
            $count = ['product_count' => 0, 'product_price' => 0];
        }

        return $count;
    }
}