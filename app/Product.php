<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = ['category_id', 'name', 'slug', 'image', 'price', 'is_active'];


    public function category(){
        return $this->belongsTo('App\Category', 'category_id');
    }
}
