<?php

namespace App\Http\Controllers\User;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index($category_slug){
        $category = Category::where('slug', $category_slug)->first();
        $data['products'] = Product::where('category_id', $category->id)->get();

        return view('user.product.index', $data);
    }

    public function view($product_slug){

        $data['product'] = Product::where('slug', $product_slug)->first();
        return view('user.product.view', $data);
    }

    public function addToCart(Request $request){

        $product = Product::find($request->product_id);

        $quantity = $request->quantity;


        if(session()->has('cart')){
            $new_item = ['product_id' => $product, 'quantity' => $quantity];
            $previous_cart = session('cart');
            $previous_cart[] = $new_item;

            session()->put('cart', $previous_cart);

        }else{

            $cart = [['product_id' => $product, 'quantity' => $quantity]];
            session()->put('cart', $cart);
        }


        session()->flash('flash_success', 'Successfullly added to cart.');

        return redirect()->back();
    }

    public function viewCart(){
        $data['cart_items'] = session('cart');
        return view('user.product.cart', $data);
    }
}
