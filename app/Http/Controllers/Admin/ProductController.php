<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index(){

        /*
         *  One -> One
         *  Category, Product (hasOne)
         *
         *
         *  One -> Many
         *  Category, Product (hasMany)
         *
         *
         *  Many -> One
         *  Product, Category (belongsTo)
         *
         *
         *  Many -> Many
         *  Category, Product (hasMany)
         *
         *
         * category
         * id , name eg : Cellphone(id = 6), Electronics(id = 7)
         *
         * product
         * id, category_id, name, price eg : Nokia 7, Iphone X, P30 Pro , Pendrive(10)
         *
         *
         * category_products
         * category_id, product_id
         * 7 , 10
         * 6, 10
         *
         *
         */




        $data['products'] = Product::with('category')->get();
        return view('admin.product.index', $data);



    }


    public function create()
    {
        $data['categories'] = Category::where('is_active', 1)->get();
        return view('admin.product.create', $data);
    }

    public function store(Request $request){
        $category = new Product();

        $insert = $request->all();
        $insert['slug'] = str_slug($request->name);

        if($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = time()."_".$image->getClientOriginalName();
            $image_path = public_path('uploads/product_images');
            if ($image->move($image_path, $image_name)) {
                $insert['image'] = $image_name;
            }
        }

        $category->fill($insert)->save();

        session()->flash('flash_success', 'Successfullly inserted');
        return redirect()->route('admin.product.index');


    }

    public function edit($id)
    {
        $data['product'] = Product::find($id);
        $data['categories'] = Category::where('is_active', 1)->get();
        return view('admin.product.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Product::find($id);
//        $category->name = $request->name;
////        $category->slug = str_slug($request->name);
//        $category->is_active = $request->is_active;
//        $category->save();


        $insert = $request->all();
        $insert['slug'] = str_slug($request->name);

        $category->fill($insert)->save();

        session()->flash('flash_success', 'Successfullly updated.');
        return redirect()->route('admin.product.index');

    }
}
