<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /*
         *
         * 1. Validation
         * 2. Login attempt
         * 3. If Login success -> redirect to dashboard else, redirect back to login.
         */

        /*
         *
         * We can also do,
         * Hash::make('amit') for bcrypt('amit')
         * Input::all() for $request->all()
         * Validator::make() $this->validate()
         * Session::put() Session::flash() , session()->flash()
         * Redirect::to(), Redirect::route(), for redirect()->route();
         */

        // Syntax for validation:
//        $this->validate($request, $rules(array), $messages(array));


        $this->validate($request, ['username' => 'required',
                                        'password' => 'required'],
                                    ['username.required' => 'No username entered.']);


        // for login we will use Auth::attempt()

        // please do remember me yourself, hint : take the value obtained from the form
        // and pass it after password
        // Syntax : Auth::attempt([array of values to be checked in user table], $remember_me);


        $auth = Auth::attempt(['email' => $request->username, 'password' => $request->password]);


        if($auth){
            return redirect()->route('admin.dashboard');
        }else{
            session()->flash('flash_error', 'Sorry, the username or password is invalid');
            return redirect()->route('admin.login');
            // or if you want to call from url, return redirect('admin/login');
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
