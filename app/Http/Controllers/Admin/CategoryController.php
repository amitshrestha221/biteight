<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['categories'] = Category::with('products')->get();

//        foreach($data['categories'] as $item){
//            echo $item->name;
//            foreach($item->products as $zz){
//                echo $zz->name;
//            }
//            echo "<br>";
//        }
//
//        dd('exit');

//        $categories = 1;
//        return view('admin.category.index', compact('categories'));

//        return view('admin.category.index', with($categories));


//        dd($data);


        return view('admin.category.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $category = new Category();
//        $category->name = $request->name;
////        $category->slug = str_slug($request->name);
//        $category->is_active = $request->is_active;
//        $category->save();


        $insert = $request->all();
        $insert['slug'] = str_slug($request->name);

        // File Upload Code

        if($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = time()."_".$image->getClientOriginalName();
            $image_path = public_path('uploads/category_images');
            if ($image->move($image_path, $image_name)) {
                $insert['image'] = $image_name;
            }
        }


        // End of File Upload Code

        $category->fill($insert)->save();

        //create
        //firstOrCreate
        //new
        //firstOrNew


        session()->flash('flash_success', 'Successfullly inserted');
        return redirect()->route('admin.category.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['category'] = Category::find($id);

        return view('admin.category.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
//        $category->name = $request->name;
////        $category->slug = str_slug($request->name);
//        $category->is_active = $request->is_active;
//        $category->save();


        $insert = $request->all();
        $insert['slug'] = str_slug($request->name);

        $category->fill($insert)->save();

        session()->flash('flash_success', 'Successfullly updated.');
        return redirect()->route('admin.category.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($category_id)
    {
        $category = Category::where('id', $category_id)->delete();
        session()->flash('flash_success', 'Status successfully deleted.');
        return redirect()->route('admin.category.index');

    }

    public function changeStatus($category_id){
        $category = Category::find($category_id);
        $category->is_active = ($category->is_active == 1) ? 0 : 1;
        $category->save();

        session()->flash('flash_success', 'Status successfully updated.');
        return redirect()->route('admin.category.index');

    }
}
