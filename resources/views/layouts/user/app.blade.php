<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="MediaCenter, Template, eCommerce">
    <meta name="robots" content="all">
    <title>Lotus premium HTML5 & CSS3 Template</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ asset('userTemplate/css/bootstrap.min.css') }}">

    <!-- Customizable CSS -->
    <link rel="stylesheet" href="{{ asset('userTemplate/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('userTemplate/css/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('userTemplate/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('userTemplate/css/owl.transitions.css') }}">
    <link rel="stylesheet" href="{{ asset('userTemplate/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('userTemplate/css/rateit.css') }}">
    <link rel="stylesheet" href="{{ asset('userTemplate/css/bootstrap-select.min.css') }}">

    <!-- Icons/Glyphs -->
    <link rel="stylesheet" href="{{ asset('userTemplate/css/font-awesome.css') }}">

    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
</head>
<body class="cnt-home">

@include('layouts.user.includes.header')

<!-- ============================================== HEADER : END ============================================== -->
<div class="body-content outer-top-vs" id="top-banner-and-menu">
    <div class="container">
        <div class="row">

            @if(!in_array(request()->segment(1), ['cart']))
                @include('layouts.user.includes.sidebar')


                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    @if(request()->segment(1) == '')
                        <div id="hero">
                            <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
                                <div class="item" style="background-image: url(userTemplate/images/sliders/01.jpg);">
                                    <div class="container-fluid">
                                        <div class="caption bg-color vertical-center text-left">
                                            <div class="slider-header fadeInDown-1">Top Brands</div>
                                            <div class="big-text fadeInDown-1"> New Collections</div>
                                            <div class="excerpt fadeInDown-2 hidden-xs"><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                                            </div>
                                            <div class="button-holder fadeInDown-3"><a
                                                        href="http://themesground.com/Lotus/V1/HTML/index.php?page=single-product"
                                                        class="btn-lg btn btn-uppercase btn-primary shop-now-button">Shop
                                                    Now</a>
                                            </div>
                                        </div>
                                        <!-- /.caption -->
                                    </div>
                                    <!-- /.container-fluid -->
                                </div>
                                <!-- /.item -->

                                <div class="item" style="background-image: url(userTemplate/images/sliders/02.jpg);">
                                    <div class="container-fluid">
                                        <div class="caption bg-color vertical-center text-left">
                                            <div class="slider-header fadeInDown-1">Spring 2016</div>
                                            <div class="big-text fadeInDown-1">Women Fashion</div>
                                            <div class="excerpt fadeInDown-2 hidden-xs"><span>Nemo enim ipsam voluptatem quia voluptas sit aspernatur.</span>
                                            </div>
                                            <div class="button-holder fadeInDown-3"><a
                                                        href="http://themesground.com/Lotus/V1/HTML/index.php?page=single-product"
                                                        class="btn-lg btn btn-uppercase btn-primary shop-now-button">Shop
                                                    Now</a>
                                            </div>
                                        </div>
                                        <!-- /.caption -->
                                    </div>
                                    <!-- /.container-fluid -->
                                </div>
                                <!-- /.item -->

                            </div>
                            <!-- /.owl-carousel -->
                        </div>
                        <div class="info-boxes wow fadeInUp">
                            <div class="info-boxes-inner">
                                <div class="row">
                                    <div class="col-md-6 col-sm-4 col-lg-4">
                                        <div class="info-box">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <h4 class="info-box-heading green">money back</h4>
                                                </div>
                                            </div>
                                            <h6 class="text">30 Days Money Back Guarantee</h6>
                                        </div>
                                    </div>
                                    <!-- .col -->

                                    <div class="hidden-md col-sm-4 col-lg-4">
                                        <div class="info-box">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <h4 class="info-box-heading green">free shipping</h4>
                                                </div>
                                            </div>
                                            <h6 class="text">Shipping on orders over $99</h6>
                                        </div>
                                    </div>
                                    <!-- .col -->

                                    <div class="col-md-6 col-sm-4 col-lg-4">
                                        <div class="info-box">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <h4 class="info-box-heading green">Special Sale</h4>
                                                </div>
                                            </div>
                                            <h6 class="text">Extra $5 off on all items </h6>
                                        </div>
                                    </div>
                                    <!-- .col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.info-boxes-inner -->

                        </div>
                    @endif
                </div>
            @endif
        </div>

        <div class="row">
        @include('common.alert')

            @if(!in_array(request()->segment(1), ['cart']))
            <!-- ============================================== SIDEBAR ============================================== -->
                <div class="col-xs-12 col-sm-12 col-md-3 sidebar">


                    <!-- ============================================== HOT DEALS ============================================== -->
                    <div class="sidebar-widget hot-deals wow fadeInUp outer-bottom-xs">
                        <h3 class="section-title">hot deals</h3>
                        <div class="owl-carousel sidebar-carousel custom-carousel owl-theme outer-top-ss">
                            <div class="item">
                                <div class="products">
                                    <div class="hot-deal-wrapper">
                                        <div class="image"><img src="userTemplate/images/hot-deals/p1.jpg" alt=""></div>
                                        <div class="sale-offer-tag"><span>49%<br>
                    off</span></div>
                                        <div class="timing-wrapper">
                                            <div class="box-wrapper">
                                                <div class="date box"><span class="key">120</span> <span
                                                            class="value">DAYS</span></div>
                                            </div>
                                            <div class="box-wrapper">
                                                <div class="hour box"><span class="key">20</span> <span
                                                            class="value">HRS</span></div>
                                            </div>
                                            <div class="box-wrapper">
                                                <div class="minutes box"><span class="key">36</span> <span
                                                            class="value">MINS</span>
                                                </div>
                                            </div>
                                            <div class="box-wrapper hidden-md">
                                                <div class="seconds box"><span class="key">60</span> <span
                                                            class="value">SEC</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.hot-deal-wrapper -->

                                    <div class="product-info text-left m-t-20">
                                        <h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
                                        <div class="rating rateit-small"></div>
                                        <div class="product-price"><span class="price"> $600.00 </span> <span
                                                    class="price-before-discount">$800.00</span></div>
                                        <!-- /.product-price -->

                                    </div>
                                    <!-- /.product-info -->

                                    <div class="cart clearfix animate-effect">
                                        <div class="action">
                                            <div class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown"
                                                        type="button"><i
                                                            class="fa fa-shopping-cart"></i></button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart
                                                </button>
                                            </div>
                                        </div>
                                        <!-- /.action -->
                                    </div>
                                    <!-- /.cart -->
                                </div>
                            </div>
                            <div class="item">
                                <div class="products">
                                    <div class="hot-deal-wrapper">
                                        <div class="image"><img src="userTemplate/images/hot-deals/p5.jpg" alt=""></div>
                                        <div class="sale-offer-tag"><span>35%<br>
                    off</span></div>
                                        <div class="timing-wrapper">
                                            <div class="box-wrapper">
                                                <div class="date box"><span class="key">120</span> <span
                                                            class="value">Days</span></div>
                                            </div>
                                            <div class="box-wrapper">
                                                <div class="hour box"><span class="key">20</span> <span
                                                            class="value">HRS</span></div>
                                            </div>
                                            <div class="box-wrapper">
                                                <div class="minutes box"><span class="key">36</span> <span
                                                            class="value">MINS</span>
                                                </div>
                                            </div>
                                            <div class="box-wrapper hidden-md">
                                                <div class="seconds box"><span class="key">60</span> <span
                                                            class="value">SEC</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.hot-deal-wrapper -->

                                    <div class="product-info text-left m-t-20">
                                        <h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
                                        <div class="rating rateit-small"></div>
                                        <div class="product-price"><span class="price"> $600.00 </span> <span
                                                    class="price-before-discount">$800.00</span></div>
                                        <!-- /.product-price -->

                                    </div>
                                    <!-- /.product-info -->

                                    <div class="cart clearfix animate-effect">
                                        <div class="action">
                                            <div class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown"
                                                        type="button"><i
                                                            class="fa fa-shopping-cart"></i></button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart
                                                </button>
                                            </div>
                                        </div>
                                        <!-- /.action -->
                                    </div>
                                    <!-- /.cart -->
                                </div>
                            </div>
                            <div class="item">
                                <div class="products">
                                    <div class="hot-deal-wrapper">
                                        <div class="image"><img src="userTemplate/images/hot-deals/p10.jpg" alt="">
                                        </div>
                                        <div class="sale-offer-tag"><span>35%<br>
                    off</span></div>
                                        <div class="timing-wrapper">
                                            <div class="box-wrapper">
                                                <div class="date box"><span class="key">120</span> <span
                                                            class="value">Days</span></div>
                                            </div>
                                            <div class="box-wrapper">
                                                <div class="hour box"><span class="key">20</span> <span
                                                            class="value">HRS</span></div>
                                            </div>
                                            <div class="box-wrapper">
                                                <div class="minutes box"><span class="key">36</span> <span
                                                            class="value">MINS</span>
                                                </div>
                                            </div>
                                            <div class="box-wrapper hidden-md">
                                                <div class="seconds box"><span class="key">60</span> <span
                                                            class="value">SEC</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.hot-deal-wrapper -->

                                    <div class="product-info text-left m-t-20">
                                        <h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
                                        <div class="rating rateit-small"></div>
                                        <div class="product-price"><span class="price"> $600.00 </span> <span
                                                    class="price-before-discount">$800.00</span></div>
                                        <!-- /.product-price -->

                                    </div>
                                    <!-- /.product-info -->

                                    <div class="cart clearfix animate-effect">
                                        <div class="action">
                                            <div class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown"
                                                        type="button"><i
                                                            class="fa fa-shopping-cart"></i></button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart
                                                </button>
                                            </div>
                                        </div>
                                        <!-- /.action -->
                                    </div>
                                    <!-- /.cart -->
                                </div>
                            </div>
                        </div>
                        <!-- /.sidebar-widget -->
                    </div>
                    <!-- ============================================== HOT DEALS: END ============================================== -->

                    <!-- ============================================== SPECIAL OFFER ============================================== -->

                    <div class="sidebar-widget outer-bottom-small wow fadeInUp">
                        <h3 class="section-title">Special Offer</h3>
                        <div class="sidebar-widget-body outer-top-xs">
                            <div class="owl-carousel sidebar-carousel special-offer custom-carousel owl-theme outer-top-xs">
                                <div class="item">
                                    <div class="products special-product">
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p30.jpg"
                                                                            alt="">
                                                                </a></div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p29.jpg"
                                                                            alt="">
                                                                </a></div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p28.jpg"
                                                                            alt="">
                                                                </a></div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="products special-product">
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p27.jpg"
                                                                            alt="">
                                                                </a></div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p26.jpg"
                                                                            alt="">
                                                                </a></div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p25.jpg"
                                                                            alt="">
                                                                </a></div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="products special-product">
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p24.jpg"
                                                                            alt="">
                                                                </a></div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p23.jpg"
                                                                            alt="">
                                                                </a></div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->
                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p22.jpg"
                                                                            alt="">
                                                                </a></div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.sidebar-widget-body -->
                    </div>
                    <!-- /.sidebar-widget -->
                    <!-- ============================================== SPECIAL OFFER : END ============================================== -->
                    <!-- ============================================== PRODUCT TAGS ============================================== -->
                    <div class="sidebar-widget product-tag wow fadeInUp">
                        <h3 class="section-title">Product tags</h3>
                        <div class="sidebar-widget-body outer-top-xs">
                            <div class="tag-list"><a class="item" title="Phone" href="category.html">Phone</a> <a
                                        class="item active" title="Vest" href="category.html">Vest</a> <a class="item"
                                                                                                          title="Smartphone"
                                                                                                          href="category.html">Smartphone</a>
                                <a class="item" title="Furniture" href="category.html">Furniture</a> <a class="item"
                                                                                                        title="T-shirt"
                                                                                                        href="category.html">T-shirt</a>
                                <a class="item" title="Sweatpants" href="category.html">Sweatpants</a> <a class="item"
                                                                                                          title="Sneaker"
                                                                                                          href="category.html">Sneaker</a>
                                <a class="item" title="Toys" href="category.html">Toys</a> <a class="item" title="Rose"
                                                                                              href="category.html">Rose</a>
                            </div>
                            <!-- /.tag-list -->
                        </div>
                        <!-- /.sidebar-widget-body -->
                    </div>
                    <!-- /.sidebar-widget -->
                    <!-- ============================================== PRODUCT TAGS : END ============================================== -->
                    <!-- ============================================== SPECIAL DEALS ============================================== -->

                    <div class="sidebar-widget outer-bottom-small wow fadeInUp">
                        <h3 class="section-title">Special Deals</h3>
                        <div class="sidebar-widget-body outer-top-xs">
                            <div class="owl-carousel sidebar-carousel special-offer custom-carousel owl-theme outer-top-xs">
                                <div class="item">
                                    <div class="products special-product">
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p28.jpg"
                                                                            alt="">
                                                                </a></div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p15.jpg"
                                                                            alt="">
                                                                </a></div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p26.jpg"
                                                                            alt="image"> </a>
                                                            </div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="products special-product">
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p18.jpg"
                                                                            alt="">
                                                                </a></div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p17.jpg"
                                                                            alt="">
                                                                </a></div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p16.jpg"
                                                                            alt="">
                                                                </a></div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->
                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="products special-product">
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p15.jpg"
                                                                            alt="images">
                                                                    <div class="zoom-overlay"></div>
                                                                </a></div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p14.jpg"
                                                                            alt="">
                                                                    <div class="zoom-overlay"></div>
                                                                </a></div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image"><a href="home.html#"> <img
                                                                            src="userTemplate/images/products/p13.jpg"
                                                                            alt="image"> </a>
                                                            </div>
                                                            <!-- /.image -->

                                                        </div>
                                                        <!-- /.product-image -->
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="home.html#">Floral Print Shirt</a>
                                                            </h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price"><span
                                                                        class="price"> $450.99 </span>
                                                            </div>
                                                            <!-- /.product-price -->

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.product-micro-row -->
                                            </div>
                                            <!-- /.product-micro -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.sidebar-widget-body -->
                    </div>
                    <!-- /.sidebar-widget -->
                    <!-- ============================================== SPECIAL DEALS : END ============================================== -->
                    <!-- ============================================== NEWSLETTER ============================================== -->
                    <div class="sidebar-widget newsletter wow fadeInUp outer-bottom-small">
                        <h3 class="section-title">Newsletters</h3>
                        <div class="sidebar-widget-body outer-top-xs">
                            <p>Sign Up for Our Newsletter!</p>
                            <form>
                                <div class="form-group">
                                    <label class="sr-only" for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                           placeholder="Subscribe to our newsletter">
                                </div>
                                <button class="btn btn-primary">Subscribe</button>
                            </form>
                        </div>
                        <!-- /.sidebar-widget-body -->
                    </div>
                    <!-- /.sidebar-widget -->
                    <!-- ============================================== NEWSLETTER: END ============================================== -->

                    <!-- ============================================== Testimonials============================================== -->
                    <div class="sidebar-widget  wow fadeInUp outer-top-vs ">
                        <div id="advertisement" class="advertisement">
                            <div class="item">
                                <div class="avatar"><img src="userTemplate/images/testimonials/member1.png" alt="Image">
                                </div>
                                <div class="testimonials"><em>"</em> Vtae sodales aliq uam morbi non sem lacus port
                                    mollis.
                                    Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                                <div class="clients_author">John Doe <span>Abc Company</span></div>
                                <!-- /.container-fluid -->
                            </div>
                            <!-- /.item -->

                            <div class="item">
                                <div class="avatar"><img src="userTemplate/images/testimonials/member3.png" alt="Image">
                                </div>
                                <div class="testimonials"><em>"</em>Vtae sodales aliq uam morbi non sem lacus port
                                    mollis.
                                    Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                                <div class="clients_author">Stephen Doe <span>Xperia Designs</span></div>
                            </div>
                            <!-- /.item -->

                            <div class="item">
                                <div class="avatar"><img src="userTemplate/images/testimonials/member2.png" alt="Image">
                                </div>
                                <div class="testimonials"><em>"</em> Vtae sodales aliq uam morbi non sem lacus port
                                    mollis.
                                    Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                                <div class="clients_author">Saraha Smith <span>Datsun &amp; Co</span></div>
                                <!-- /.container-fluid -->
                            </div>
                            <!-- /.item -->

                        </div>
                        <!-- /.owl-carousel -->
                    </div>

                    <!-- ============================================== Testimonials: END ============================================== -->


                    <div class="home-banner"><img src="userTemplate/images/banners/LHS-banner.jpg" alt="Image"></div>
                </div>
                <!-- /.sidemenu-holder -->
                <!-- ============================================== SIDEBAR : END ============================================== -->

            @endif
            @yield('content')
        </div>
        <!-- /.row -->
        <!-- ============================================== BRANDS CAROUSEL ============================================== -->
        <div id="brands-carousel" class="logo-slider wow fadeInUp">
            <div class="logo-slider-inner">
                <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
                    <div class="item m-t-15"><a href="home.html#" class="image"> <img
                                    data-echo="userTemplate/images/brands/brand1.png"
                                    src="userTemplate/images/blank.gif" alt="">
                        </a></div>
                    <!--/.item-->

                    <div class="item m-t-10"><a href="home.html#" class="image"> <img
                                    data-echo="userTemplate/images/brands/brand2.png"
                                    src="userTemplate/images/blank.gif" alt="">
                        </a></div>
                    <!--/.item-->

                    <div class="item"><a href="home.html#" class="image"> <img
                                    data-echo="userTemplate/images/brands/brand3.png"
                                    src="userTemplate/images/blank.gif" alt="">
                        </a></div>
                    <!--/.item-->

                    <div class="item"><a href="home.html#" class="image"> <img
                                    data-echo="userTemplate/images/brands/brand4.png"
                                    src="userTemplate/images/blank.gif" alt="">
                        </a></div>
                    <!--/.item-->

                    <div class="item"><a href="home.html#" class="image"> <img
                                    data-echo="userTemplate/images/brands/brand5.png"
                                    src="userTemplate/images/blank.gif" alt="">
                        </a></div>
                    <!--/.item-->

                    <div class="item"><a href="home.html#" class="image"> <img
                                    data-echo="userTemplate/images/brands/brand6.png"
                                    src="userTemplate/images/blank.gif" alt="">
                        </a></div>
                    <!--/.item-->

                    <div class="item"><a href="home.html#" class="image"> <img
                                    data-echo="userTemplate/images/brands/brand2.png"
                                    src="userTemplate/images/blank.gif" alt="">
                        </a></div>
                    <!--/.item-->

                    <div class="item"><a href="home.html#" class="image"> <img
                                    data-echo="userTemplate/images/brands/brand4.png"
                                    src="userTemplate/images/blank.gif" alt="">
                        </a></div>
                    <!--/.item-->

                    <div class="item"><a href="home.html#" class="image"> <img
                                    data-echo="userTemplate/images/brands/brand1.png"
                                    src="userTemplate/images/blank.gif" alt="">
                        </a></div>
                    <!--/.item-->

                    <div class="item"><a href="home.html#" class="image"> <img
                                    data-echo="userTemplate/images/brands/brand5.png"
                                    src="userTemplate/images/blank.gif" alt="">
                        </a></div>
                    <!--/.item-->
                </div>
                <!-- /.owl-carousel #logo-slider -->
            </div>
            <!-- /.logo-slider-inner -->

        </div>
        <!-- /.logo-slider -->
        <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
    </div>
    <!-- /.container -->
</div>
<!-- /#top-banner-and-menu -->

<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Contact Us</h4>
                    </div>
                    <!-- /.module-heading -->

                    <div class="module-body">
                        <ul class="toggle-footer" style="">
                            <li class="media">
                                <div class="pull-left"><span class="icon fa-stack fa-lg"> <i
                                                class="fa fa-map-marker fa-stack-1x fa-inverse"></i> </span></div>
                                <div class="media-body">
                                    <p>Themesstock, 789 Main rd, Anytown, CA 12345 USA</p>
                                </div>
                            </li>
                            <li class="media">
                                <div class="pull-left"><span class="icon fa-stack fa-lg"> <i
                                                class="fa fa-mobile fa-stack-1x fa-inverse"></i> </span></div>
                                <div class="media-body">
                                    <p>+(888) 123-4567<br>
                                        +(888) 456-7890</p>
                                </div>
                            </li>
                            <li class="media">
                                <div class="pull-left"><span class="icon fa-stack fa-lg"> <i
                                                class="fa fa-envelope fa-stack-1x fa-inverse"></i> </span></div>
                                <div class="media-body"><span><a href="home.html#">lotus@themesstock.com</a></span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- /.module-body -->
                </div>
                <!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Customer Service</h4>
                    </div>
                    <!-- /.module-heading -->

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first"><a href="home.html#" title="Contact us">My Account</a></li>
                            <li><a href="home.html#" title="About us">Order History</a></li>
                            <li><a href="home.html#" title="faq">FAQ</a></li>
                            <li><a href="home.html#" title="Popular Searches">Specials</a></li>
                            <li class="last"><a href="home.html#" title="Where is my order?">Help Center</a></li>
                        </ul>
                    </div>
                    <!-- /.module-body -->
                </div>
                <!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Corporation</h4>
                    </div>
                    <!-- /.module-heading -->

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first"><a title="Your Account" href="home.html#">About us</a></li>
                            <li><a title="Information" href="home.html#">Customer Service</a></li>
                            <li><a title="Addresses" href="home.html#">Company</a></li>
                            <li><a title="Addresses" href="home.html#">Investor Relations</a></li>
                            <li class="last"><a title="Orders History" href="home.html#">Advanced Search</a></li>
                        </ul>
                    </div>
                    <!-- /.module-body -->
                </div>
                <!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Why Choose Us</h4>
                    </div>
                    <!-- /.module-heading -->

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first"><a href="home.html#" title="About us">Shopping Guide</a></li>
                            <li><a href="home.html#" title="Blog">Blog</a></li>
                            <li><a href="home.html#" title="Company">Company</a></li>
                            <li><a href="home.html#" title="Investor Relations">Investor Relations</a></li>
                            <li class=" last"><a href="http://themesground.com/Lotus/V1/HTML/contact-us.html"
                                                 title="Suppliers">Contact Us</a></li>
                        </ul>
                    </div>
                    <!-- /.module-body -->
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-bar">
        <div class="container">
            <div class="col-xs-12 col-sm-6 no-padding social">
                <ul class="link">
                    <li class="fb pull-left"><a target="_blank" rel="nofollow" href="home.html#" title="Facebook"></a>
                    </li>
                    <li class="tw pull-left"><a target="_blank" rel="nofollow" href="home.html#" title="Twitter"></a>
                    </li>
                    <li class="googleplus pull-left"><a target="_blank" rel="nofollow" href="home.html#"
                                                        title="GooglePlus"></a></li>
                    <li class="rss pull-left"><a target="_blank" rel="nofollow" href="home.html#" title="RSS"></a></li>
                    <li class="pintrest pull-left"><a target="_blank" rel="nofollow" href="home.html#"
                                                      title="PInterest"></a></li>
                    <li class="linkedin pull-left"><a target="_blank" rel="nofollow" href="home.html#"
                                                      title="Linkedin"></a></li>
                    <li class="youtube pull-left"><a target="_blank" rel="nofollow" href="home.html#"
                                                     title="Youtube"></a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 no-padding">
                <div class="clearfix payment-methods">
                    <ul>
                        <li><img src="userTemplate/images/payments/1.png" alt=""></li>
                        <li><img src="userTemplate/images/payments/2.png" alt=""></li>
                        <li><img src="userTemplate/images/payments/3.png" alt=""></li>
                        <li><img src="userTemplate/images/payments/4.png" alt=""></li>
                        <li><img src="userTemplate/images/payments/5.png" alt=""></li>
                    </ul>
                </div>
                <!-- /.payment-methods -->
            </div>
        </div>
    </div>
</footer>
<!-- ============================================================= FOOTER : END============================================================= -->

<!-- For demo purposes – can be removed on production -->

<!-- For demo purposes – can be removed on production : End -->

<!-- JavaScripts placed at the end of the document so the pages load faster -->
<script src="{{ asset('userTemplate/js/jquery-1.11.1.min.js') }}"></script>
<script src="{{ asset('userTemplate/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('userTemplate/js/bootstrap-hover-dropdown.min.js') }}"></script>
<script src="{{ asset('userTemplate/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('userTemplate/js/echo.min.js') }}"></script>
<script src="{{ asset('userTemplate/js/jquery.easing-1.3.min.js') }}'"></script>
<script src="{{ asset('userTemplate/js/bootstrap-slider.min.js') }}"></script>
<script src="{{ asset('userTemplate/js/jquery.rateit.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('userTemplate/js/lightbox.min.js') }}"></script>
<script src="{{ asset('userTemplate/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('userTemplate/js/wow.min.js') }}"></script>
<script src="{{ asset('userTemplate/js/scripts.js') }}"></script>
</body>
</html>