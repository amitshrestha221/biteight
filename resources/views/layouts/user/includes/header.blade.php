<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1">

    <!-- ============================================== TOP MENU ============================================== -->
    <div class="top-bar animate-dropdown">
        <div class="container">
            <div class="header-top-inner">
                <div class="cnt-account">
                    <ul class="list-unstyled">
                        <li><a href="home.html#">My Account</a></li>
                        <li><a href="home.html#">Wishlist</a></li>
                        <li><a href="home.html#">My Cart</a></li>
                        <li><a href="home.html#">Checkout</a></li>
                        <li><a href="home.html#">Login</a></li>
                    </ul>
                </div>
                <!-- /.cnt-account -->

                <div class="cnt-block">
                    <ul class="list-unstyled list-inline">
                        <li class="dropdown dropdown-small"><a href="home.html#" class="dropdown-toggle"
                                                               data-hover="dropdown" data-toggle="dropdown"><span
                                        class="value">USD </span><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="home.html#">USD</a></li>
                                <li><a href="home.html#">INR</a></li>
                                <li><a href="home.html#">GBP</a></li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-small"><a href="home.html#" class="dropdown-toggle"
                                                               data-hover="dropdown" data-toggle="dropdown"><span
                                        class="value">English </span><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="home.html#">English</a></li>
                                <li><a href="home.html#">French</a></li>
                                <li><a href="home.html#">German</a></li>
                            </ul>
                        </li>
                    </ul>
                    <!-- /.list-unstyled -->
                </div>
                <!-- /.cnt-cart -->
                <div class="offer-text">Save up to 12% everyday on all products</div>
                <div class="clearfix"></div>
            </div>
            <!-- /.header-top-inner -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.header-top -->
    <!-- ============================================== TOP MENU : END ============================================== -->
    <div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
                    <!-- ============================================================= LOGO ============================================================= -->
                    <div class="logo"><a href="home.html"> <img src="assets/images/logo.png" alt="logo"> </a></div>
                    <!-- /.logo -->
                    <!-- ============================================================= LOGO : END ============================================================= -->
                </div>
                <!-- /.logo-holder -->

                <div class="col-xs-12 col-sm-12 col-md-6 top-search-holder">
                    <!-- /.contact-row -->
                    <!-- ============================================================= SEARCH AREA ============================================================= -->
                    <div class="search-area">
                        <form>
                            <div class="control-group">
                                <ul class="categories-filter animate-dropdown">
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"
                                                            href="category.html">Categories <b class="caret"></b></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li class="menu-header">Computer</li>
                                            <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                       href="category.html">- Clothing</a></li>
                                            <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                       href="category.html">- Electronics</a></li>
                                            <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                       href="category.html">- Shoes</a></li>
                                            <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                       href="category.html">- Watches</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <input class="search-field" placeholder="Search here..."/>
                                <a class="search-button" href="home.html#"></a></div>
                        </form>
                    </div>
                    <!-- /.search-area -->
                    <!-- ============================================================= SEARCH AREA : END ============================================================= -->
                </div>
                <!-- /.top-search-holder -->

                <div class="col-xs-12 col-sm-12 col-md-3 animate-dropdown top-cart-row">
                    <!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->
                    @php $count = \App\Classes\Helper::getCartCount(); @endphp

                    <div class="dropdown dropdown-cart"><a href="{{ route('product.viewCart') }}" class="dropdown-toggle lnk-cart"
                                                           >
                            <div class="items-cart-inner">
                                <div class="top-cart"></div>

                                <div class="total-price-basket"><span class="lbl">{{ (isset($count['product_count'])) ?  $count['product_count'] : 0 }} items /</span> <span
                                            class="total-price"> <span class="sign">$</span><span
                                                class="value">{{ $count['product_price'] ?? 0}}</span> </span></div>
                            </div>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="cart-item product-summary">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="image"><a href="detail.html"><img src="assets/images/cart.jpg"
                                                                                          alt=""></a></div>
                                        </div>
                                        <div class="col-xs-7">
                                            <h3 class="name"><a
                                                        href="http://themesground.com/Lotus/V1/HTML/index.php?page-detail">Simple
                                                    Product</a></h3>
                                            <div class="price">$600.00</div>
                                        </div>
                                        <div class="col-xs-1 action"><a href="home.html#"><i
                                                        class="fa fa-trash"></i></a></div>
                                    </div>
                                </div>
                                <!-- /.cart-item -->
                                <div class="clearfix"></div>
                                <hr>
                                <div class="clearfix cart-total">
                                    <div class="pull-right"><span class="text">Sub Total :</span><span class='price'>$600.00</span>
                                    </div>
                                    <div class="clearfix"></div>
                                    <a href="checkout.html"
                                       class="btn btn-upper btn-primary btn-block m-t-20">Checkout</a></div>
                                <!-- /.cart-total-->

                            </li>
                        </ul>
                        <!-- /.dropdown-menu-->
                    </div>
                    <!-- /.dropdown-cart -->

                    <!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= -->
                </div>
                <!-- /.top-cart-row -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->

    </div>
    <!-- /.main-header -->

    <!-- ============================================== NAVBAR ============================================== -->
    <div class="header-nav animate-dropdown">
        <div class="container">
            <div class="yamm navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse"
                            class="navbar-toggle collapsed" type="button">
                        <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span
                                class="icon-bar"></span> <span class="icon-bar"></span></button>
                </div>
                <div class="nav-bg-class">
                    <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
                        <div class="nav-outer">
                            <ul class="nav navbar-nav">
                                <li class="active dropdown yamm-fw"><a href="home.html" data-hover="dropdown"
                                                                       class="dropdown-toggle" data-toggle="dropdown">Home</a>
                                </li>
                                <li class="dropdown yamm mega-menu"><a href="home.html" data-hover="dropdown"
                                                                       class="dropdown-toggle" data-toggle="dropdown">Clothing</a>
                                    <ul class="dropdown-menu container">
                                        <li>
                                            <div class="yamm-content ">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6 col-md-2 col-menu">
                                                        <h2 class="title">Men</h2>
                                                        <ul class="links">
                                                            <li><a href="home.html#">Dresses</a></li>
                                                            <li><a href="home.html#">Shoes </a></li>
                                                            <li><a href="home.html#">Jackets</a></li>
                                                            <li><a href="home.html#">Sunglasses</a></li>
                                                            <li><a href="home.html#">Sport Wear</a></li>
                                                            <li><a href="home.html#">Blazers</a></li>
                                                            <li><a href="home.html#">Shirts</a></li>
                                                        </ul>
                                                    </div>
                                                    <!-- /.col -->

                                                    <div class="col-xs-12 col-sm-6 col-md-2 col-menu">
                                                        <h2 class="title">Women</h2>
                                                        <ul class="links">
                                                            <li><a href="home.html#">Handbags</a></li>
                                                            <li><a href="home.html#">Jwellery</a></li>
                                                            <li><a href="home.html#">Swimwear </a></li>
                                                            <li><a href="home.html#">Tops</a></li>
                                                            <li><a href="home.html#">Flats</a></li>
                                                            <li><a href="home.html#">Shoes</a></li>
                                                            <li><a href="home.html#">Winter Wear</a></li>
                                                        </ul>
                                                    </div>
                                                    <!-- /.col -->

                                                    <div class="col-xs-12 col-sm-6 col-md-2 col-menu">
                                                        <h2 class="title">Boys</h2>
                                                        <ul class="links">
                                                            <li><a href="home.html#">Toys & Games</a></li>
                                                            <li><a href="home.html#">Jeans</a></li>
                                                            <li><a href="home.html#">Shirts</a></li>
                                                            <li><a href="home.html#">Shoes</a></li>
                                                            <li><a href="home.html#">School Bags</a></li>
                                                            <li><a href="home.html#">Lunch Box</a></li>
                                                            <li><a href="home.html#">Footwear</a></li>
                                                        </ul>
                                                    </div>
                                                    <!-- /.col -->

                                                    <div class="col-xs-12 col-sm-6 col-md-2 col-menu">
                                                        <h2 class="title">Girls</h2>
                                                        <ul class="links">
                                                            <li><a href="home.html#">Sandals </a></li>
                                                            <li><a href="home.html#">Shorts</a></li>
                                                            <li><a href="home.html#">Dresses</a></li>
                                                            <li><a href="home.html#">Jwellery</a></li>
                                                            <li><a href="home.html#">Bags</a></li>
                                                            <li><a href="home.html#">Night Dress</a></li>
                                                            <li><a href="home.html#">Swim Wear</a></li>
                                                        </ul>
                                                    </div>
                                                    <!-- /.col -->

                                                    <div class="col-xs-12 col-sm-6 col-md-4 col-menu banner-image"><img
                                                                class="img-responsive"
                                                                src="assets/images/banners/top-menu-banner.jpg" alt="">
                                                    </div>
                                                    <!-- /.yamm-content -->
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown mega-menu">
                                    <a href="category.html" data-hover="dropdown" class="dropdown-toggle"
                                       data-toggle="dropdown">Electronics <span class="menu-label hot-menu hidden-xs">hot</span>
                                    </a>
                                    <ul class="dropdown-menu container">
                                        <li>
                                            <div class="yamm-content">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-2 col-menu">
                                                        <h2 class="title">Laptops</h2>
                                                        <ul class="links">
                                                            <li><a href="home.html#">Gaming</a></li>
                                                            <li><a href="home.html#">Laptop Skins</a></li>
                                                            <li><a href="home.html#">Apple</a></li>
                                                            <li><a href="home.html#">Dell</a></li>
                                                            <li><a href="home.html#">Lenovo</a></li>
                                                            <li><a href="home.html#">Microsoft</a></li>
                                                            <li><a href="home.html#">Asus</a></li>
                                                            <li><a href="home.html#">Adapters</a></li>
                                                            <li><a href="home.html#">Batteries</a></li>
                                                            <li><a href="home.html#">Cooling Pads</a></li>
                                                        </ul>
                                                    </div>
                                                    <!-- /.col -->

                                                    <div class="col-xs-12 col-sm-12 col-md-2 col-menu">
                                                        <h2 class="title">Desktops</h2>
                                                        <ul class="links">
                                                            <li><a href="home.html#">Routers & Modems</a></li>
                                                            <li><a href="home.html#">CPUs, Processors</a></li>
                                                            <li><a href="home.html#">PC Gaming Store</a></li>
                                                            <li><a href="home.html#">Graphics Cards</a></li>
                                                            <li><a href="home.html#">Components</a></li>
                                                            <li><a href="home.html#">Webcam</a></li>
                                                            <li><a href="home.html#">Memory (RAM)</a></li>
                                                            <li><a href="home.html#">Motherboards</a></li>
                                                            <li><a href="home.html#">Keyboards</a></li>
                                                            <li><a href="home.html#">Headphones</a></li>
                                                        </ul>
                                                    </div>
                                                    <!-- /.col -->

                                                    <div class="col-xs-12 col-sm-12 col-md-2 col-menu">
                                                        <h2 class="title">Cameras</h2>
                                                        <ul class="links">
                                                            <li><a href="home.html#">Accessories</a></li>
                                                            <li><a href="home.html#">Binoculars</a></li>
                                                            <li><a href="home.html#">Telescopes</a></li>
                                                            <li><a href="home.html#">Camcorders</a></li>
                                                            <li><a href="home.html#">Digital</a></li>
                                                            <li><a href="home.html#">Film Cameras</a></li>
                                                            <li><a href="home.html#">Flashes</a></li>
                                                            <li><a href="home.html#">Lenses</a></li>
                                                            <li><a href="home.html#">Surveillance</a></li>
                                                            <li><a href="home.html#">Tripods</a></li>
                                                        </ul>
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col-xs-12 col-sm-12 col-md-2 col-menu">
                                                        <h2 class="title">Mobile Phones</h2>
                                                        <ul class="links">
                                                            <li><a href="home.html#">Apple</a></li>
                                                            <li><a href="home.html#">Samsung</a></li>
                                                            <li><a href="home.html#">Lenovo</a></li>
                                                            <li><a href="home.html#">Motorola</a></li>
                                                            <li><a href="home.html#">LeEco</a></li>
                                                            <li><a href="home.html#">Asus</a></li>
                                                            <li><a href="home.html#">Acer</a></li>
                                                            <li><a href="home.html#">Accessories</a></li>
                                                            <li><a href="home.html#">Headphones</a></li>
                                                            <li><a href="home.html#">Memory Cards</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-4 col-menu custom-banner"><a
                                                                href="home.html#"><img alt=""
                                                                                       src="assets/images/banners/banner-side.png"></a>
                                                    </div>
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.yamm-content --> </li>
                                    </ul>
                                </li>
                                <li class="dropdown hidden-sm"><a href="category.html">Health & Beauty <span
                                                class="menu-label new-menu hidden-xs">new</span> </a></li>
                                <li class="dropdown hidden-sm"><a href="category.html">Watches</a></li>
                                <li class="dropdown"><a href="http://themesground.com/Lotus/V1/HTML/contact.html">Jewellery</a>
                                </li>
                                <li class="dropdown"><a
                                            href="http://themesground.com/Lotus/V1/HTML/contact.html">Shoes</a></li>
                                <li class="dropdown"><a href="http://themesground.com/Lotus/V1/HTML/contact.html">Kids &
                                        Girls</a></li>
                                <li class="dropdown"><a href="home.html#" class="dropdown-toggle" data-hover="dropdown"
                                                        data-toggle="dropdown">Pages</a>
                                    <ul class="dropdown-menu pages">
                                        <li>
                                            <div class="yamm-content">
                                                <div class="row">
                                                    <div class="col-xs-12 col-menu">
                                                        <ul class="links">
                                                            <li><a href="home.html">Home</a></li>
                                                            <li><a href="category.html">Category</a></li>
                                                            <li><a href="detail.html">Detail</a></li>
                                                            <li><a href="shopping-cart.html">Shopping Cart Summary</a>
                                                            </li>
                                                            <li><a href="checkout.html">Checkout</a></li>
                                                            <li><a href="blog.html">Blog</a></li>
                                                            <li><a href="blog-details.html">Blog Detail</a></li>
                                                            <li>
                                                                <a href="http://themesground.com/Lotus/V1/HTML/contact.html">Contact</a>
                                                            </li>
                                                            <li><a href="sign-in.html">Sign In</a></li>
                                                            <li><a href="my-wishlist.html">Wishlist</a></li>
                                                            <li><a href="terms-conditions.html">Terms and Condition</a>
                                                            </li>
                                                            <li><a href="track-orders.html">Track Orders</a></li>
                                                            <li><a href="product-comparison.html">Product-Comparison</a>
                                                            </li>
                                                            <li><a href="faq.html">FAQ</a></li>
                                                            <li><a href="404.html">404</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown  navbar-right special-menu"><a href="home.html#">Todays offer</a>
                                </li>
                            </ul>
                            <!-- /.navbar-nav -->
                            <div class="clearfix"></div>
                        </div>
                        <!-- /.nav-outer -->
                    </div>
                    <!-- /.navbar-collapse -->

                </div>
                <!-- /.nav-bg-class -->
            </div>
            <!-- /.navbar-default -->
        </div>
        <!-- /.container-class -->

    </div>
    <!-- /.header-nav -->
    <!-- ============================================== NAVBAR : END ============================================== -->

</header>