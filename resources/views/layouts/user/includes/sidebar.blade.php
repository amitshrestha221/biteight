<div class="col-xs-12 col-sm-12 col-md-3 sidebar">
    <!-- ================================== TOP NAVIGATION ================================== -->
    <div class="side-menu animate-dropdown">
        <div class="head"><i class="icon fa fa-bars"></i> Categories</div>
        <nav class="yamm megamenu-horizontal">
            <ul class="nav">


                @php
                    $categories = \App\Classes\Helper::getCategories();
                @endphp

                @foreach($categories as $cat)
                    <li class="dropdown menu-item">
                        <a href="{{ route('product.list', $cat->slug) }}" class="dropdown-toggle">
                            {{ $cat->name }}
                        </a>
                    </li>
            @endforeach
            <!-- /.menu-item -->
            </ul>
            <!-- /.nav -->
        </nav>
        <!-- /.megamenu-horizontal -->
    </div>
    <!-- /.side-menu -->
</div>