@extends('layouts.user.app')

@section('content')

    <!-- ============================================== CONTENT ============================================== -->
    <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder">
        <!-- ========================================== SECTION – HERO ========================================= -->



        <!-- ============================================== WIDE PRODUCTS : END ============================================== -->
        <!-- ============================================== SCROLL TABS ============================================== -->
        <div id="product-tabs-slider" class="scroll-tabs outer-top-vs wow fadeInUp">
            <div class="more-info-tab clearfix ">
                <h3 class="new-product-title pull-left">New Products</h3>
                <ul class="nav nav-tabs nav-tab-line pull-right" id="new-products-1">
                    <li class="active"><a data-transition-type="backSlide" href="home.html#all"
                                          data-toggle="tab">All</a></li>
                    <li><a data-transition-type="backSlide" href="home.html#smartphone" data-toggle="tab">Clothing</a>
                    </li>
                    <li><a data-transition-type="backSlide" href="home.html#laptop" data-toggle="tab">Electronics</a>
                    </li>
                    <li><a data-transition-type="backSlide" href="home.html#apple" data-toggle="tab">Shoes</a>
                    </li>
                </ul>
                <!-- /.nav-tabs -->
            </div>
            <div class="tab-content outer-top-xs">
                <div class="tab-pane in active" id="all">
                    <div class="product-slider">
                        <div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="4">
                            @foreach($products as $item)
                            <div class="item item-carousel">
                                <div class="products">
                                    <div class="product">
                                        <div class="product-image">
                                            <div class="image">
                                                <a href="{{ route('product.view', $item->slug) }}"><img
                                                            src="{{ asset('uploads/product_images/'.$item->image) }}" alt=""></a>
                                            </div>
                                            <!-- /.image -->

                                            <div class="tag new"><span>new</span></div>
                                        </div>
                                        <!-- /.product-image -->

                                        <div class="product-info text-left">
                                            <h3 class="name"><a href="{{ route('product.view', $item->slug) }}">{{ $item->name }}</a>
                                            </h3>
                                            <div class="rating rateit-small"></div>
                                            <div class="description"></div>
                                            <div class="product-price"><span class="price"> {{ $item->price }} </span>

                                            <!-- /.product-price -->

                                        </div>
                                        <!-- /.product-info -->
                                        <div class="cart clearfix animate-effect">
                                            <div class="action">
                                                <ul class="list-unstyled">
                                                    <li class="add-cart-button btn-group">
                                                        <button data-toggle="tooltip"
                                                                class="btn btn-primary icon" type="button"
                                                                title="Add Cart"><i
                                                                    class="fa fa-shopping-cart"></i></button>
                                                        <button class="btn btn-primary cart-btn" type="button">
                                                            Add to cart
                                                        </button>
                                                    </li>
                                                    <li class="lnk wishlist"><a data-toggle="tooltip"
                                                                                class="add-to-cart"
                                                                                href="detail.html"
                                                                                title="Wishlist"> <i
                                                                    class="icon fa fa-heart"></i> </a></li>
                                                    <li class="lnk"><a data-toggle="tooltip" class="add-to-cart"
                                                                       href="detail.html" title="Compare"> <i
                                                                    class="fa fa-signal" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.action -->
                                        </div>
                                        <!-- /.cart -->
                                    </div>
                                    <!-- /.product -->

                                </div>
                                <!-- /.products -->
                            </div>
                            @endforeach
                            <!-- /.item -->
                        </div>
                        <!-- /.home-owl-carousel -->
                    </div>
                    <!-- /.product-slider -->
                </div>


            </div>
            <!-- /.tab-content -->
        </div>


    </div>
    <!-- /.homebanner-holder -->
    <!-- ============================================== CONTENT : END ============================================== -->

@endsection