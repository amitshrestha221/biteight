@extends('layouts.admin.app')


@section('content')


    <div class="card">
        <div class="card-header">
            <h2>Categories <small>Add Category
                </small></h2>
        </div>

        <div class="card-body card-padding">
            <form method="post" action="{{ route('admin.category.store') }}" role="form" enctype="multipart/form-data">
                <div class="form-group fg-line">
                    <label for="exampleInputEmail1">Name</label>
                    <input name="name" type="text" class="form-control input-sm" id="exampleInputEmail1" placeholder="Enter category name">
                </div>

                <div class="form-group fg-line">
                    <label for="exampleInputEmail1">Image</label>
                    <input name="image" type="file" class="form-control input-sm" id="exampleInputEmail1">
                </div>

                <div class="form-group fg-line">
                        <p class="f-500 m-b-15 c-black">Status</p>

                        <select name="is_active" class="selectpicker">
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                        </select>
                </div>
                <button type="submit" class="btn btn-primary btn-sm m-t-10">Submit</button>
                {!! csrf_field() !!}
            </form>
        </div>
    </div>


@endsection