@extends('layouts.admin.app')


@section('content')


    <div class="card">
        <div class="card-header">
            <h2>Categories <small>Edit Category
                </small></h2>
        </div>

        <div class="card-body card-padding">
       Edit Category
            <form method="post" action="{{ route('admin.category.update', $category->id) }}" role="form">
                <div class="form-group fg-line">
                    <label for="exampleInputEmail1">Name</label>
                    <input name="name" type="text" class="form-control input-sm" id="exampleInputEmail1" placeholder="Enter category name" value="{{ $category->name }}">
                </div>
                <div class="form-group fg-line">
                    <p class="f-500 m-b-15 c-black">Status</p>

                    <select name="is_active" class="selectpicker">
                        <option value="1" {{ ($category->is_active == 1) ? "selected" : "" }}>Active</option>
                        <option value="0" {{ ($category->is_active == 1) ? "selected" : "" }}>Inactive</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary btn-sm m-t-10">Submit</button>
                {!! csrf_field() !!}
                {{--<input type="hidden" name="category_id" value="{{ $category->id }}">--}}
            </form>
        </div>
    </div>


@endsection