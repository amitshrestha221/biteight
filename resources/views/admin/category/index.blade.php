@extends('layouts.admin.app')

@section('content')


    <div class="card">
        <div class="card-header">
            <h2>Categories
                <small>List of categories</small>
            </h2>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('admin.category.create') }}">Create New</a>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($categories as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->name }}</td>
                        <td>
                            <a href="{{ route('admin.category.changeStatus', $item->id) }}" class="{{ ($item->is_active == 1) ? "btn btn-success" : "btn btn-danger" }}">
                                {{ ($item->is_active == 1) ? "Active" : "Inactive" }}
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-primary" href="{{ route('admin.category.edit', [$item->id]) }}">Edit</a>
                            <a class="btn btn-danger" href="{{ route('admin.category.delete', [$item->id]) }}">Delete</a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>


@endsection