@extends('layouts.admin.app')


@section('content')


    <div class="card">
        <div class="card-header">
            <h2>Products <small>Edit Product
                </small></h2>
        </div>

        <div class="card-body card-padding">
            <form method="post" action="{{ route('admin.product.update', $product->id) }}" role="form">

                <div class="form-group fg-line">
                    <label for="exampleInputEmail1">Category</label>

                    <select name="category_id">
                        <option value="">Select a category</option>
                        @foreach($categories as $item)
                            <option value="{{ $item->id }}" {{ ($product->category_id  == $item->id) ? "selected" : "" }}>{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group fg-line">
                    <label for="exampleInputEmail1">Name</label>
                    <input name="name" type="text" class="form-control input-sm" id="exampleInputEmail1" placeholder="Enter product name" value="{{ $product->name }}">
                </div>

                <div class="form-group fg-line">
                    <label for="exampleInputEmail1">Price</label>
                    <input name="price" type="text" class="form-control input-sm" id="exampleInputEmail1" placeholder="Enter product price" value="{{ $product->price }}">
                </div>
                <div class="form-group fg-line">
                    <p class="f-500 m-b-15 c-black">Status</p>

                    <select name="is_active" class="selectpicker">
                        <option value="1" {{ ($product->is_active == 1) ? "selected" : "" }}>Active</option>
                        <option value="0" {{ ($product->is_active == 1) ? "selected" : "" }}>Inactive</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary btn-sm m-t-10">Submit</button>
                {!! csrf_field() !!}
            </form>
        </div>
    </div>


@endsection