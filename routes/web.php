<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix('admin')->namespace('Admin')->group(function () {
    Route::get('/', 'LoginController@index')->name('admin.login');

    Route::get('login', 'LoginController@index')->name('admin.login');
    Route::post('login/submit', 'LoginController@store')->name('admin.login.submit');

    Route::middleware('admin')->group(function () {
        Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');

        Route::prefix('category')->group(function () {
            Route::get('/', 'CategoryController@index')->name('admin.category.index');
            Route::get('create', 'CategoryController@create')->name('admin.category.create');
            Route::post('store', 'CategoryController@store')->name('admin.category.store');
            Route::get('changeStatus/{category_id}', 'CategoryController@changeStatus')->name('admin.category.changeStatus');
            Route::get('delete/{category_id}', 'CategoryController@destroy')->name('admin.category.delete');

            Route::get('edit/{category_id}', 'CategoryController@edit')->name('admin.category.edit');
            Route::post('update/{category_id}', 'CategoryController@update')->name('admin.category.update');
        });

        Route::prefix('product')->group(function () {
            Route::get('/', 'ProductController@index')->name('admin.product.index');
            Route::get('create', 'ProductController@create')->name('admin.product.create');
            Route::post('store', 'ProductController@store')->name('admin.product.store');
//            Route::get('changeStatus/{category_id}', 'CategoryController@changeStatus')->name('admin.category.changeStatus');
//            Route::get('delete/{category_id}', 'CategoryController@destroy')->name('admin.category.delete');
//
            Route::get('edit/{category_id}', 'ProductController@edit')->name('admin.product.edit');
            Route::post('update/{category_id}', 'ProductController@update')->name('admin.product.update');
        });
    });


    /*
     * In previous versions, used to call route like this.
     */

    /*Route::post('login/submit', [
        'as' => 'admin.login.submit',
        'uses' => 'LoginController@store'
    ]);*/


});


Route::namespace('User')->group(function(){
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('product/{category_slug}', 'ProductController@index')->name('product.list');


    Route::get('product/view/{product_slug}', 'ProductController@view')->name('product.view');

    Route::post('product/addToCart', 'ProductController@addToCart')->name('product.addToCart');

    Route::get('cart', 'ProductController@viewCart')->name('product.viewCart');

});
